\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {1}Curve di Landau}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Perdita di energia di particelle nella materia}{2}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Distribuzione di Landau}{4}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Sorgente e decadimento beta}{5}{subsubsection.1.1.2}
\contentsline {subsection}{\numberline {1.2}Apparato sperimentale}{5}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Rivelatore al silicio}{5}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Studio delle strips del rivelatore a silicio}{6}{subsubsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}Catena elettronica}{7}{subsubsection.1.2.3}
\contentsline {subsubsection}{\numberline {1.2.4}Discriminatore}{8}{subsubsection.1.2.4}
\contentsline {subsubsection}{\numberline {1.2.5}Unit\IeC {\`a} di coincidenza}{9}{subsubsection.1.2.5}
\contentsline {subsubsection}{\numberline {1.2.6}Dual timer}{9}{subsubsection.1.2.6}
\contentsline {subsubsection}{\numberline {1.2.7}Unit\IeC {\`a} di ritardo}{10}{subsubsection.1.2.7}
\contentsline {subsubsection}{\numberline {1.2.8}Multichannel Analyzer}{10}{subsubsection.1.2.8}
\contentsline {subsection}{\numberline {1.3}Singolo Piano}{11}{subsection.1.3}
\contentsline {subsubsection}{\numberline {1.3.1}Acquisizione dati}{11}{subsubsection.1.3.1}
\contentsline {subsubsection}{\numberline {1.3.2}Analisi Dati}{13}{subsubsection.1.3.2}
\contentsline {subsection}{\numberline {1.4}Multi-Piano}{15}{subsection.1.4}
\contentsline {subsubsection}{\numberline {1.4.1}Acquisizione dati}{15}{subsubsection.1.4.1}
\contentsline {subsubsection}{\numberline {1.4.2}Analisi Dati}{15}{subsubsection.1.4.2}
\contentsline {subsection}{\numberline {1.5}Simulazione}{17}{subsection.1.5}
\contentsline {section}{\numberline {2}Decadimento del positronio}{19}{section.2}
\contentsline {subsection}{\numberline {2.1}Apparato sperimentale}{20}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Sorgente}{20}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Rivelatori a scintillazione e fotomoltiplicatori}{22}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}CFD}{23}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}TAC e MCA}{23}{subsubsection.2.1.4}
\contentsline {subsection}{\numberline {2.2}Esperimento}{24}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Impostazione delle soglie}{24}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Calibrazione Temporale con sorgente di cobalto}{26}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Analisi Dati}{27}{subsubsection.2.2.3}
\contentsline {section}{Appendice \numberline {A}Curva di Landau}{30}{Appendice.alph1.Alph1}

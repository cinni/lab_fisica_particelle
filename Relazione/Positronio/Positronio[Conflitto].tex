\section{Decadimento del positronio}

L'atomo di positronio Ps è un atomo idrogenoide costituito da uno stato legato tra positrone ed elettrone.
Il positrone $e^{+}$ è l’antiparticella dell’elettrone $e^{-}$, esse possono interagire elettromagneticamente, portando alla formazione del positronio.\\
I livelli energetici del positronio si differenziano da quelli di un atomo d'idrogeno per il fatto che la massa del positrone e del potrone sono diverse, mentre il comportamento dal punto di vista dell'interazione elettromagnetica è il medesimo. Dunque, la massa ridotta da inserire nell'equazione di Schrödinger per il positronio è $\mu = m_{e}/2$ , mentre nel caso dell'atomo di idrogeno è $\mu = m_{e}$, ciò comporta che anche le frequenze associate alle linee spettrali siano ridotte di un fattore $1/2$ rispetto a quelle dell'idrogeno.\\
Il positronio può quindi presentarsi in due diverse configurazioni, in base alle relative orientazioni di spin di elettrone e positrone, una con $S_{tot} = 0$ oppure $S_{tot} = 1$:

\begin{itemize}
\item  Para-positronio ($p-Ps$): stato di singoletto (S=0) con spin antiparalleli . Tale stato ha una vita media nel vuoto di $\SI{125}{ps}$ e decade in un numero pari di fotoni.
\item Orto-positronio ($o-Ps$): stato di tripletto  (S=1) con spin paralleli (3 possibili configurazioni). Tale stato ha vita media nel vuoto di $\SI{140}{ns}$ e decade in un numero dispari di fotoni, principalmente in tre $\gamma$.
\end{itemize}

L'annichilazione può avvenire tra le due particelle costituenti il positronio, oppure quando all'interno di un materiale può accadere che l’orto-positronio si leghi a un elettrone del materiale con spin antiparallelo e si trasformi in para-positronio, il quale successivamente annichila in due fotoni da $\SI{511}{keV}$, questo processo è detto di $pickoff$.\\

In questo esperimento andremo a misurare la vita media dell'orto-positronio, dal momento che è maggiore di quella del para-positronio. Il valore che ci aspettiamo di ottenere è minore di quello nel vuoto poiché andiamo a stimare la vita media in un mezzo.\\
Lo spettro temporale è il risultato di una sopravrapposizione di esponenziali decrescenti con il tempo, caratterizzate da tempi di vita media differenti. Questo perchè ci troviamo in un materiale plastico, se il decadimento fosse avvenuto in un cristallo perfetto invece avremmo ottenuto una poissoniana. La presenza di difetti, come vacanze o cavità, che determinano una variazione nella densità elettronica del materiale, comporta che i positroni che annichilano in cavità o difetti, dove la densità elettronica è inferiore rispetto al resto del materiale, lo facciano con tempi più lunghi poiché avviene il fenomeno del $pickoff$. 

\subsection{Setup sperimentale}
Per misurare la vita media del positronio è necessario misurare il tempo che intercorre tra la creazione del positrone e la sua annichilazione, distinguendo tra annichilazione del positrone senza formazione dello stato legato del positronio e decadimento dello stato legato del positronio. La creazione positrone corrisponde al segnale di start della misura ee l'annichilazione a uno di stop. Si è utilizzata una sorgente di $^{22}\text{Na}$ che decade in uno stato eccitato del Neon ($^{22}\text{Ne}^{*}$) attraverso un decadimento di tipo $\beta^{+}$, emettendo così il positrone che si annichilerà con un elettrone o formerà del positronio nell’involucro di plexiglass in cui è contenuta la sorgente. Il neon a sua volta si rilassa quasi istantaneamente (nell’ordine di qualche ps) emettendo un raggio $\gamma$ di energia \SI{1,274}{MeV}. Questo fotone viene utilizzato come segnale di start, ad indicare la creazione del positronio; il segnale di stop invece è dato da uno dei due fotoni emessi dall’annichilazione $e^{+}e^{-}$, arrestando l’acquisizione. I raggi vengono rivelati grazie a due scintillatori collineari (uno dedicato al segnale di start e l’altro al segnale di stop) accoppiati a due fotomoltiplicatori, che convertono il segnale luminoso in segnale elettrico. I segnali in uscita dai fotomoltiplicatori passano in un CFD che restituisce in uscita un segnale logico se quello in entrata si trova in un range determinato da due soglie che vengono da noi determinate. Le onde quadre generate dai CFD vengono poi mandate ad un TAC (time to amplitude converter) che invia il segnale in uscita a un MCA (multichannel analyzer) che misura l’ampiezza del segnale in funzione dei canali. Infine il MCA è collegato ad un computer per acquisire il segnale misurato. Per ottenere una misura in tempo è poi necessario calibrare il sistema, utilizzando come sorgente il $^{60}Co$ che decade $\beta^{-}$ emettendo due $\gamma$ in coincidenza.
Il setup sperimentale utilizzato è il seguente:

\begin{itemize}
	\item due rivelatori a scintillazione e due fototubi (PMT);
	\item un alimentatore ad alta tensione per i PMT;
	\item un attenuatore;
	\item un modulo fan-in fan-out;
	\item due CFD (constant fraction discriminator);
	\item un modulo di ritardo;
	\item un TAC (time to analog converter);
	\item un MCA (multichannel analyzer).
\end{itemize}

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}[auto, node distance=3cm and 1cm,>=latex',  L/.style = {draw, red, -{Stealth[scale=3,length=2.5,width=2]}}, T/.style = {draw, rounded corners, to path={|- (\tikztotarget)}}, T1/.style = {draw, rounded corners, to path={-| (\tikztotarget)}}]
	
	\node [block] (scintillatore) {Rivelatore};
	\node [block, right of=scintillatore] (fot) {PMT + Att};
	\node [block, right of=fot] (cfd) {CFD};
	\node [block, right of=cfd] (tac) {TAC};
	\node [block, right of=tac] (mca) {MCA};
	
	
	\draw [->] (scintillatore)--(fot);
	\draw [->] (fot)--(cfd);
	\draw [->] (cfd)--(tac);
	\draw [->] (tac)--(mca);
	
	\node[block, below of=cfd](cfd2){CFD};
	\node[block, below of=fot](fot2){PMT};
	\node[block, below of=scintillatore](scintillatore2){Rivelatore};
	\draw [->] (scintillatore2)--(fot2);
	\draw [->] (fot2)--(cfd2);
	\draw [->] (cfd2) edge[T1] (tac);
	
	\end{tikzpicture}
	\caption{Catena elettronica per la misura della vita media del positronio.}
	\label{fig:vita_media_positronio}
\end{figure}

\subsubsection{Sorgente}
La sorgente utilizzata è il $^{22}\text{Na}$, il quale decade in uno stato eccitato del Neon ($^{22}\text{Ne}^{*}$) con branching ratio del $90,5 \%$, producendo un positrone ad un energia massima di $\SI{0,546}{MeV}$, con il seguente decadimento $\beta^{+}$:
\begin{equation}
^{22}\text{Na} \rightarrow ^{22}\text{Ne}^{*} + e^{+}+{\nu}_{e}
\end{equation}

Il positrone prodotto interagisce con un elettrone atomico della materia circostante, nel nostro caso è l'involucro di plexiglass in cui è avvolta la sorgente, in due possibili modi:
\begin{itemize}
	\item annichilazione immediata con due $\gamma$ da $\SI{511}{keV}$;
	\item formazione di uno stato legato con l'elettrone a formare il positronio.
\end{itemize}

Il $^{22}\text{Ne}^{*}$ si diseccita tramite emissione di un $\gamma$ di $\SI{1274.53}{keV}$ dopo $\SI{3.6}{ps}$ dalla sua produzione.\\
Per andare a misurare la vita media del positronio, dalla sua formazione al suo decadimento, ci servono un segnale di START e uno di STOP.
La diseccitazione del $^{22}\text{Ne}^{*}$ può essere considerata istantanea, poiché ci aspettiamo che la vita media del positronio sia dell'ordine del nanosecondo, ed essere utilizzata come segnale di formazione di un atomo di positronio.
La rivelazione di uno dei due $\gamma$ da $\SI{511}{keV}$ di decadimento del positronio fungerà invece da segnale di STOP.

\begin{figure}[h!tb]
	\centerline {\includegraphics[scale=0.6]{decadimento_sodio.png}}
	\caption{Decadimento del Sodio}
	\label{fig:deca_sodio}	
\end{figure}

La sorgente di $^{22}\text{Na}$ ha un tempo di dimezzamento pari a $t = \SI{2,6}{anni}$ e quella da noi utilizzata ha un’attività calibrata al 1/8/2003 pari a $\SI{395}{kBq}$.
Si può stimare l’attività attuale tramite:

\begin{equation}
A(t) = A_{0} \exp{\dfrac{-\Delta t}{\tau}} 
\label{attivita}
\end{equation}

dove $A(t)$ è l'attività della sortgente al momento $t$ attuale, $A_{0}$ è l'attività della sorgente al momento $t_{0}$ che ci viene fornita, $\Delta t$ è l'intervallo di tempo trascorso tra $t_{0}$ e $t$, $\tau$ è la vita media del nuclide.
Svolgendo i conti si è stimata una attività attuale di $\SI{1,5}{kBq}$          \textcolor{red}{ricontrollo conti}

La sorgente utilizzata per la calibrazione è il $^{60}\text{Co}$. Esso decade per decadimento $\beta^{-}$ nell’isotopo eccitato $^{60}\text{Ni}^{4+}$ con Branching Ratio del $ 99,88\% $, che tende a raggiungere lo stato fondamentale emettendo due $\gamma$, il primo con energia di $\SI{1,17}{MeV}$ (passando allo stato $^{60}\text{Ni}^{2+}$) e il secondo con energia $\SI{1,33}{MeV}$ . 
Tali $\gamma$ possono considerarsi in coincidenza in quanto la catena di diseccitazioni avviene in un intervallo di tempo inferiore alla risoluzione temporale del nostro rivelatore.  
Il $^{60}\text{Co}$ ha tempo di dimezzamento $t = \SI{5,271}{anni}$  e l'attività iniziale (1/10/1978) è di  $\SI{370}{kBq}$ , dunque sfruttando l'equazione \vref{attivita} si trova che l’attività attuale risulta pari a $\SI{24,8}{kBq}$.  \textcolor{red}{da calcolare kBq}.

\begin{figure}[h!tb]
	\centerline {\includegraphics[scale=0.7]{decadimento_cobalto.png}}
	\caption{Decadimento del cobalto}
	\label{fig:deca_cobalto}	
\end{figure}


\subsubsection{Rivelatori a scintillazione e fotomoltiplicatori}

L'apparato di rivelazione è costituito da due scintillatori collineari (uno dedicato al segnale di START e l’altro al segnale di STOP) accoppiati a due fotomoltiplicatori, che convertono il segnale luminoso in segnale elettrico.\\
Il rivelatore a scintillazione è formato da un cristallo i cui atomi si eccitano/ionizzano al passaggio della radiazione; gli atomi del cristallo, per tornare allo stato fondamentale, emettono radiazione nello spettro della luce visibile e ultravioletta tramite il processo di fluorescenza. \\
Questa luce viene poi raccolta e trasformata in segnale elettrico da dei fototubi. I fototubi convertono linearmente i fotoni in fotoelettroni attraverso un fotocatodo posto all'ingresso della struttura cilindrica che compone il fototubo. Successivamente i fotoelettroni vengono moltiplicati attraverso una serie di dinodi, poiché il segnale iniziale dato dai soli fotoni è troppo debole.\\

I rivelatori a scintillazione usati nell'esperimento sono rivelatori a Floruro di Bario ($BaF_{2}$) di diverso spessore: quello più spesso è più efficiente nel rivelare i fotoni più energetici. Il rivelatore a Floruro di Bario è un buono scintillatore per produzione di luce e linearità, in più ha un'alta densità ($\SI{4.9}{\g \per \centi\meter^{3}}$) che gli conferisce un’alta efficienza.\\
I due fotomoltiplicatori sono alimentati con una tensione di $\SI{-2}{kV}$.
Poiché il fotomoltiplicatore collegato al rivelatore sottile presenta segnali troppo elevati, ne è stata attenuata l’uscita, per poter così impostare le soglie del CFD.

\subsubsection{CFD}

Un discriminatore a frazione costante è uno strumento che dà un segnale in uscita quando il segnale di ingresso raggiunge una certa frazione del massimo e non quando supera una certa soglia. 
Infatti i segnali in uscita degli scintillatori hanno tempi di risalita identici che sono maggiori della risoluzione temporale desiderata. Questo non permette di fare un trigger temporale su una soglia fissata perchè dipenderebbe dall'altezza del picco del segnale. Tempi di risalita identici e stesse forme del segnale consentono di fare il trigger non a soglie fissate ma a frazioni costanti del picco, rendendo il trigger sul tempo indipendente dalle altezze.\\
Questo consente a segnali prodotti allo stesso istante temporale di raggiungere il trigger nello stesso istante nonostante i segnali siano di altezze diverse.
In questo modo è possibile selezionare $\gamma$ di un certo intervallo energetico impostando un LOWER LEVEL e un UPPER LEVEL.
Il CFD ha diverse uscite che permettono di avere diverse forme del segnale in uscita: l'uscita BK OUT forma un segnale rettangolare di larghezza regolabile, quella TIMING forma un segnale \textcolor{red}{con picco}

\begin{figure}[h!tb]
	\centerline {\includegraphics[scale=0.3]{CFD.png}}
	\caption{Principio di funzionamento di un CFD}
	\label{fig:CFD}	
\end{figure}

\subsubsection{TAC e MCA}
Il dispositivo TAC (Time to Amplitude Converter) permette di effettuare misure di tempo, come quella necessaria a noi per misurare la vita media del positronio. Esso prende in ingresso due segnali, che fungono da START e STOP, e restituisce in uscita un segnale in tensione proporzionale al tempo trascorso tra l’arrivo del segnale di START, dato dal segnale proveniente dal rivelatore spesso e quello di STOP, dato dal segnale proveniente dal rivelatore sottile.
L'uscita della TAC viene collegata al MCA attraverso l'ingresso V IN e impostata su INTERNAL TRIGGER.

\subsection{Esperimento}

\subsubsection{Impostazione delle soglie}
La prima operazione fatta è stata quella di impostare correttamente le soglie del CFD in modo da misurare solo i $\gamma$ desiderati.
Con il rivelatore spesso andiamo a rivelare i $\gamma$ dati dalla diseccitazione del $^{22}\text{Ne}^{*}$, mentre con il rivelatore sottile quelli dati dall'annichilazione del positronio, i primi infatti sono più energetici dei secondi.
I $\gamma$ interagiscono nei nostri rivelatori soprattutto per effetto Compton, ed essendo la probabilità di interazioni multiple praticamente per via delle dimensioni dei rivelatori, quello che si ottiene negli spettri energetici sono le spalle Compton dei due $\gamma$.
Innanzitutto andiamo a misurare lo spettro del $^{22}\text{Na}$ con i due rivelatori, per fare questa misura abbiamo sdoppiato il segnale con un modulo di FAN-IN FAN-OUT: un'uscita viene mandata al CFD che sfruttando l'uscita BK OUT fornisce un segnale rettangolare di larghezza regolabile, l'altra viene mandata a un modulo di ritardo in modo tale che il segnale in uscita dal fotomoltiplicatore sia contenuto in quello del CFD. Entrambi i segnali vengono mandati al MCA, il segnale proveniente dal CFD nell'ingresso EXIT TRIGGER, quello proveniente dai fotomoltiplicatori in Q IN. Durante questa operazione le soglie sono aperte, in modo da misurare tutti i $\gamma$.

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}[auto, node distance=3cm and 1cm,>=latex',  L/.style = {draw, red, -{Stealth[scale=3,length=3,width=2]}}, T/.style = {draw, rounded corners, to path={|- (\tikztotarget)}}, T1/.style = {draw, rounded corners, to path={-| (\tikztotarget)}}]
	
	\node [block] (fotomoltipl) {Fotomoltipl.};
	\node [block, right of=fotomoltipl] (fan) {Fan};
	\node [block, right of=fan] (discr) {Discr.};
	\node [block, right of=discr] (mca) {MCA};
	
	
	\draw [->] (fotomoltipl)--(fan);
	\draw [->] (fan)--(discr);
	\draw [->] (discr)--(mca);
%	\draw[->] ([yshift= 0.3 cm]discr.east) to  ([yshift= 0.3 cm]coin.west) ;
%	\draw[->] ([yshift= -0.3 cm]discr.east) to  ([yshift= -0.3 cm]coin.west) ;
%	
%	\draw[->] ([yshift= 0.3 cm]preamp.east) to  ([yshift= 0.3 cm]discr.west) ;
%	\draw[->] ([yshift= -0.3 cm]preamp.east) to  ([yshift= -0.3 cm]discr.west) ;
	
%	\draw[->] (coin) -- (mca);
	%\draw (preamp) -- (3,-1.5);
	%\draw (3,-1.5) -- (12,-1.5);
	%\draw[->] (12,-1.5) -- (mca);
	%\draw (preamp) to[line to] ;
	
%	\node[block, below of=mca](dt){Dual Timer};
	\node[block, below of=discr](rit){ritardo};
	\draw (fan) edge[T, ->] (rit);
	\draw (rit) edge[T1, ->] (mca);
	
	
	\end{tikzpicture}
	\caption{Collegamenti delle unità della catena elettronica.}
	\label{fig:catena}
\end{figure}


Lo spettro del rivelatore sottile mostra una spalle Compton che corrisponde ai $\gamma$ alle energie di \SI{511}{keV}; essi forniranno il segnale di STOP poiché dovuti all'annichilazione del positronio.\\
Lo spettro del rivelatore spesso mostra due spalle Compton, una a più basse energia dovuta all'annichilazione del positronio e una a più alta energia sono dovuta alla diseccitazione del $^{22}\text{Ne}^{*}$ che rappresenta il segnale di START.\\

A causa delle esigue dimensioni dei rivelatori non sono presenti fotopicchi.


%\begin{figure}[htbp]
%	\centering
%	\begin{tikzpicture}
%	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=0]
%	\addplot [smooth, color=blue] file {dati_positronio/sottile.txt};
%	\end{axis}
%	\end{tikzpicture}
%	\caption{Spettro ottenuto da rivelatore sottile}
%	\label{plot:rivelatore_sottile}
%\end{figure}

%\begin{figure}[htbp]
%	\centering
%	\begin{tikzpicture}
%	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=0, xmax=1200]
%	\addplot [smooth, color=blue] file {dati_positronio/sottile_traslato.txt};
%	\end{axis}
%	\end{tikzpicture}
%	\caption{Spettro ottenuto da rivelatore sottile traslato}
%	\label{plot:rivelatore_sottile_traslato}
%\end{figure}

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}
	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=0, xmax=1000]
	
	\fill[blue!40]
	(axis cs:170,-73) -- 
	(axis cs:330,-73) --
	(axis cs:330,750) --
	(axis cs:170,750) --
	cycle;
	
	\addplot [smooth, color=blue] file {dati_positronio/sottile2.txt};
	\end{axis}
	\end{tikzpicture}
	\caption{Spettro ottenuto da rivelatore sottile2}
	\label{plot:rivelatore_sottile2}
\end{figure}


\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}
	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=0, xmax=600]
	\fill[blue!40]
	(axis cs:100,-98) -- 
	(axis cs:250, -98) --
	(axis cs:250,1390) --
	(axis cs:100,1390) --
	cycle;
	\addplot [smooth, color=blue] file {dati_positronio/spesso.txt};
	\end{axis}
	\end{tikzpicture}
	\caption{Spettro ottenuto da rivelatore spesso}
	\label{plot:rivelatore_spesso}
\end{figure}


\subsubsection{Calibrazione Temporale con sorgente di Cobalto}
Per ottenere una misura di tempo è necessario convertire i canali in tempo.
Per fare la calibrazione canali-tempo abbiamo utilizzato una sorgente di $^{60}\text{Co}$, il quale decade emettendo due $\gamma$ che si possono considerare in concidenza , in quando distanziati di un tempo dell'ordine dei picosecondi.
Acquisiamo i vari spettri impostando, attraverso un unità di ritardo, i seguenti ritardi \SI{0}{ns}, \SI{5}{ns}, \SI{9}{ns}, \SI{13}{ns}, \SI{17}{ns}.\\
Ogni spettro viene fittato un una gaussiana, in corrispondenza del picco della quale individuo il canale, come mostrato in figure ~\vref{plot:cobalto_gaus}; successivamente i picchi sono stati fittati con una retta per ottenere la conversione canali-energia, come riportato nel grafico ~\vref{plot:cobalto_fit}. Otteniamo la seguente retta di calibrazione:

\begin{equation}
\text{canale} = 50 \cdot \text{t}[\si{\nano\second}] + 1038
\label{retta_calibrazione}
\end{equation}

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}
	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=900]
	\addplot [smooth] file {dati_positronio/cobalto1.txt};
	\addplot [smooth] file {dati_positronio/cobalto2.txt};
	\addplot [smooth] file {dati_positronio/cobalto3.txt};
	\addplot [smooth] file {dati_positronio/cobalto4.txt};
	\addplot [smooth] file {dati_positronio/cobalto5.txt};
	\addplot [smooth] file {dati_positronio/cobalto6.txt};
	\addplot [smooth] file {dati_positronio/cobalto7.txt};
	
	\addplot[smooth, red, domain=900:1200, samples=100] {gauss(23.6357, 1052.98, 30.6518)};
	\addplot[smooth, yellow, domain=1000:1150, samples=100] {gauss(28.294, 1084.61, 14.5769)};
	\addplot[smooth, blue, domain=1050:1250, samples=100] {gauss(19.2567, 1136.19, 20.152)};
	\addplot[smooth, green, domain=1200:1400, samples=100] {gauss(14.8074, 1286.94, 22.5453)};
	\addplot[smooth, pink, domain=1450:1650, samples=100] {gauss(15.3132, 1538.31, 21.1838)};
	\addplot[smooth, cyan, domain=1600:1800, samples=100] {gauss(22.8803, 1686.77, 18.1683)};
	\addplot[smooth, magenta, domain=1700:2000, samples=100] {gauss(18.0869, 1838.4, 18.9137)};
	\end{axis}
	\end{tikzpicture}
	\caption{Fit gaussiani per i $\gamma$ del Cobalto con ritardi di \SI{0}{ns},\SI{5}{ns},\SI{9}{ns},\SI{13}{ns},\SI{17}{ns}.}
	\label{plot:cobalto_gaus}
\end{figure}

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}
	\begin{axis}[xlabel=$\Delta t$, ylabel= Channel, , xmin=-1, xmax=17]
	\addplot [smooth, only marks, mark=o,error bars/.cd, y dir=both,y explicit] table[x=x, y=y, y error=erry] {dati_positronio/Cobalto_Fit/COBALTO.dat};
	
	\addplot[smooth, red, domain=0:16, samples=100] {50*x + 1038};
	\end{axis}
	\end{tikzpicture}
	\caption{Retta di calibrazione canali-tempo con sorgente di Cobalto.}
	\label{plot:cobalto_fit}
\end{figure}

\subsubsection{Analisi Dati}
Fissate le soglie abbiamo lanciato la misura della vita media dell'ortopositronio.
La catena elettronica utilizzata nell'acquisizione dati è quella riportata in figure \vref{}, nei CDF si sono utilizzate le uscite TIMING OUTPUTS. Il segnale in uscita dai due discriminatori è stato dunque mandato ad un modulo TAC il quale fornisce in uscita un segnale di ampiezza proporzionale alla distanza temporale tra i due segnali in ingresso. Il segnale in uscita dal TAC è stato collegato all’entrata V IN del MCA, il quale è stato impostato su INTERNAL TRIGGER.\\
Lo spettro ottenuto viene analizzato attraverso RooFit; in particolare fittiamo la curva con la somma di due esponenziali date rispettivamente dal caso in cui $e^{+}e^{-}$ annichilino senza produrre il positronio e dal caso in cui $e^{+}e^{-}$ producano positronio e con una funzione gaussiana che tiene conto della risposta temporale del rivelatore.
II fit totale è dato dalla convoluzione della somma delle due esponenziali con la gaussiana.

\begin{equation}
y(t)=\int_{-\infty}^{+\infty} dt' \, \left( Ae^{-\frac{t'}{\tau_{1}}} + Be^{-\frac{t'}{\tau_{2}}} \right) G(t',t,\sigma)
\end{equation}

Abbiamo lasciato come parametri liberi $A$, $B$, $\tau_{1}$, $\tau_{2}$ e $\sigma$.

Il fit ottenuto è riportato in figura \vref{fig:positronio}.
\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.6]{positronio_canali.pdf}
	\caption{Fit dello spettro temporale del positronio.}
	\label{fig:positronio}
\end{figure}	

Il fit è stato effettuato sui canali ed è stata poi effettuata la conversione in tempo dei parametri $\tau_{1}$ e $\tau_{2}$.
I risultati ottenuti per $\tau_{1}$ e $\tau_{2}$ sono:

$$\tau_{1}=0,464\pm 0,006 ns$$
$$\tau_{2}=0,910\pm 0,007 ns$$

dove il peso relativo delle due componenti è pari a 0,87.









\section{Decadimento del positronio}

L'atomo di positronio (Ps) è un atomo idrogenoide costituito da uno stato legato tra positrone ed elettrone.
Il positrone $e^{+}$ è l’antiparticella dell’elettrone $e^{-}$, esse possono interagire elettromagneticamente, portando alla formazione di uno stato legato: il positronio.\\
I livelli energetici del positronio si differenziano da quelli di un atomo di idrogeno per il fatto che la massa del positrone e del protone sono uguali, mentre il comportamento dal punto di vista dell'interazione elettromagnetica è il medesimo. Dunque, la massa ridotta da inserire nell'equazione di Schrödinger per il positronio è $\mu = m_{e}/2$ , mentre nel caso dell'atomo di idrogeno è $\mu = m_{e}$, ciò comporta che anche le frequenze associate alle linee spettrali siano ridotte di un fattore $1/2$ rispetto a quelle dell'idrogeno.\\
\begin{equation}
E_{n}=-\dfrac{\mu q^{4}_{e}}{8h^{2}\epsilon^{2}_{0}}\dfrac{1}{n^{2}}=-\dfrac{6,8}{n^2} \si{\electronvolt}
\end{equation}

Il positronio può presentarsi in due diverse configurazioni, in base alle relative orientazioni di spin di elettrone e positrone, una con $S_{tot} = 0$ oppure $S_{tot} = 1$:

\begin{itemize}
\item  Para-positronio (p-Ps): stato di singoletto (S=0) con spin antiparalleli . Tale stato ha una vita media nel vuoto di $\SI{125}{ps}$ e decade in un numero pari di fotoni.
\item Orto-positronio (o-Ps): stato di tripletto  (S=1) con spin paralleli (3 possibili configurazioni). Tale stato ha vita media nel vuoto di $\SI{140}{ns}$ e decade in un numero dispari di fotoni, principalmente in tre $\gamma$.
\end{itemize}

Il decadimento all'interno di un materiale polimerico può accadere nei seguenti modi:
\begin{itemize}
	\item decadimento del para-positronio (troppo breve per essere misurato in questo esperimento);
	\item annichilazione di positroni nelle catena polmeriche e in difetti strutturali tipici di un materiale amorfo $\tau \sim (300 \div 400)~\si{\pico\second}$;
	\item decadimento dell'orto-positronio tramite reazione di \textit{pickoff}.
\end{itemize}


All'interno del materiale può accadere infatti che l’orto-positronio si leghi a un elettrone del materiale con spin antiparallelo e si trasformi in para-positronio, il quale successivamente annichila in due fotoni da $\SI{511}{keV}$, questo processo è detto di \textit{pickoff}.\\

In questo esperimento andremo a misurare la vita media del positronio all'interno di un materiale. 
Lo spettro temporale è il risultato di una sopravrapposizione di due esponenziali decrescenti, caratterizzati da costanti di tempo differenti. 
La minore corrisponte all'annichilazione dei positroni all'interno dei difetti del materiale (vacanze o cavità), la maggiore al decadimento tramite reazione di \textit{pickoff}.
 
\subsection{Apparato sperimentale}
Per misurare la vita media del positronio è necessario misurare il tempo che intercorre tra la creazione del positrone e la sua annichilazione. La creazione positrone corrisponde al segnale di start della misura e l'annichilazione a quello di stop. Si è utilizzata una sorgente di $^{22}\text{Na}$ che decade in uno stato eccitato del Neon ($^{22}\text{Ne}^{*}$) attraverso un decadimento di tipo $\beta^{+}$, emettendo così il positrone che si annichilerà con un elettrone o formerà del positronio nell’involucro di plexiglass in cui è contenuta la sorgente. Il Neon a sua volta si rilassa quasi istantaneamente (nell’ordine di qualche ps) emettendo un raggio $\gamma$ di energia \SI{1,274}{MeV}. Questo fotone viene utilizzato come segnale di start, ad indicare la creazione del positrone; il segnale di stop invece è dato da uno dei due fotoni emessi dall’annichilazione $e^{+}e^{-}$.
Il setup sperimentale utilizzato (mostrato in figura \vref{fig:vita_media_positronio}) è il seguente:
\begin{itemize}
	\item Due rivelatori a scintillazione e due fototubi (PMT)
	\item Alimentatore ad alta tensione per i PMT;	
	\item Attenuatore;
	\item Modulo fan-in fan-out
	\item Due CFD (Constant Fraction Discriminator)
	\item Modulo di ritardo
	\item TAC (Time to Amplitude Converter)
	\item MCA (Multichannel Analyzer)
\end{itemize}

I fotoni emessi vengono rivelati grazie a due scintillatori collineari (uno dedicato al segnale di start e l’altro al segnale di stop) accoppiati a due fotomoltiplicatori, che convertono il segnale luminoso in segnale elettrico. I segnali in uscita dai fotomoltiplicatori passano in un CFD (Constant Fraction Discriminator) che restituisce in uscita un segnale logico se quello in entrata si trova in un range determinato da due soglie che vengono da noi settate. Le onde quadre generate dai CFD vengono poi mandate ad un TAC (Time to Amplitude Converter) che invia il segnale in uscita a un MCA (Multichannel Analyzer) che misura l’ampiezza del segnale in funzione dei canali. Infine il MCA è collegato ad un computer per acquisire il segnale misurato. Per ottenere una misura in tempo è poi necessario calibrare il sistema, utilizzando come sorgente il $^{60}\text{Co}$ che decade $\beta^{-}$ emettendo due $\gamma$ in coincidenza.



\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}[auto, node distance=3cm and 1cm,>=latex',  L/.style = {draw, red, -{Stealth[scale=3,length=2.5,width=2]}}, T/.style = {draw, rounded corners, to path={|- (\tikztotarget)}}, T1/.style = {draw, rounded corners, to path={-| (\tikztotarget)}}]
	
	\node [block] (scintillatore) {Rivelatore};
	\node [block, right of=scintillatore] (fot) {PMT + Att};
	\node [block, right of=fot] (cfd) {CFD};
	\node [block, right of=cfd] (tac) {TAC};
	\node [block, right of=tac] (mca) {MCA};
	
	
	\draw [->] (scintillatore)--(fot);
	\draw [->] (fot)--(cfd);
	\draw [->] (cfd)--(tac);
	\draw [->] (tac)--(mca);
	
	\node[block, below of=cfd](cfd2){CFD};
	\node[block, below of=fot](fot2){PMT};
	\node[block, below of=scintillatore](scintillatore2){Rivelatore};
	\draw [->] (scintillatore2)--(fot2);
	\draw [->] (fot2)--(cfd2);
	\draw [->] (cfd2) edge[T1] (tac);
	
	\end{tikzpicture}
	\caption{Catena elettronica per la misura della vita media del positronio.}
	\label{fig:vita_media_positronio}
\end{figure}

\subsubsection{Sorgente}
La sorgente utilizzata è il $^{22}\text{Na}$, il quale decade in uno stato eccitato del Neon ($^{22}\text{Ne}^{*}$) con branching ratio del $90,5 \%$, producendo un positrone ad un energia massima di $\SI{0,546}{MeV}$, con il seguente decadimento $\beta^{+}$:
\begin{equation}
^{22}\text{Na} \rightarrow ^{22}\text{Ne}^{*} + e^{+}+{\nu}_{e}
\end{equation}

Il $^{22}\text{Ne}^{*}$ si diseccita tramite emissione di un $\gamma$ di $\SI{1274.53}{keV}$ dopo $\SI{3.6}{ps}$ dalla sua produzione.\\
La diseccitazione del $^{22}\text{Ne}^{*}$ può essere considerata istantanea, poiché ci aspettiamo che la vita media del positronio sia dell'ordine del nanosecondo, ed essere utilizzata come segnale di formazione del positronio.
La rivelazione di uno dei due $\gamma$ da $\SI{511}{keV}$ di decadimento del positronio fungerà invece da segnale di STOP.

\begin{figure}[h!tb]
	\centerline {\includegraphics[scale=0.4]{decadimento_sodio.png}}
	\caption{Decadimento del Sodio}
	\label{fig:deca_sodio}	
\end{figure}

La sorgente di $^{22}\text{Na}$ ha un tempo di dimezzamento pari a $t = \SI{2,6}{anni}$ e quella da noi utilizzata ha un’attività calibrata al 1/8/2003 pari a $\SI{395}{kBq}$.
Si può stimare l’attività attuale tramite:

\begin{equation}
A(t) = A_{0} \exp{-\dfrac{\Delta t}{\tau}} 
\label{attivita}
\end{equation}

dove $A(t)$ è l'attività della sortgente al momento $t$ attuale, $A_{0}$ è l'attività della sorgente al momento $t_{0}$ che ci viene fornita, $\Delta t$ è l'intervallo di tempo trascorso tra $t_{0}$ e $t$ e $\tau$ è la vita media del nuclide.
Svolgendo i conti si è stimata una attività attuale di $\SI{8.26}{\kilo\becquerel}$.

La sorgente utilizzata per la calibrazione è il $^{60}\text{Co}$. Esso decade per decadimento $\beta^{-}$ con branching ratio del $99,88\% $ nell’isotopo eccitato $^{60}\text{Ni}^{4+}$, che tende a raggiungere lo stato fondamentale emettendo due $\gamma$, il primo con energia di $\SI{1,17}{MeV}$ (passando allo stato $^{60}\text{Ni}^{2+}$) e il secondo con energia $\SI{1,33}{MeV}$ . 
Tali $\gamma$ possono considerarsi in coincidenza in quanto la catena di diseccitazioni avviene in un intervallo di tempo inferiore alla risoluzione temporale del nostro rivelatore.  
Il $^{60}\text{Co}$ ha tempo di dimezzamento $t = \SI{5,271}{anni}$  e l'attività iniziale (1/10/1978) è di $\SI{370}{kBq}$, dunque sfruttando l'equazione \vref{attivita} si trova che l’attività attuale risulta pari a $\SI{2.10}{\kilo\becquerel}$.  

\begin{figure}[h!tb]
	\centerline {\includegraphics[scale=0.5]{decadimento_cobalto.PNG}}
	\caption{Decadimento del cobalto.}
	\label{fig:deca_cobalto}	
\end{figure}


\subsubsection{Rivelatori a scintillazione e fotomoltiplicatori}

L'apparato di rivelazione è costituito da due scintillatori collineari (uno dedicato al segnale di START e l’altro al segnale di STOP) accoppiati a due fotomoltiplicatori, che convertono il segnale luminoso in segnale elettrico.\\
Il rivelatore a scintillazione è formato da un cristallo i cui atomi si eccitano/ionizzano al passaggio della radiazione; gli atomi del cristallo, per tornare allo stato fondamentale, emettono radiazione nello spettro della luce visibile e ultravioletta tramite il processo di fluorescenza. \\
Questa luce viene poi raccolta e trasformata in segnale elettrico da dei fototubi. I fototubi convertono linearmente i fotoni in fotoelettroni attraverso un fotocatodo posto all'ingresso della struttura cilindrica che compone il fototubo. Successivamente i fotoelettroni vengono moltiplicati attraverso una serie di dinodi, poiché il segnale iniziale dato dai soli fotoni è troppo debole.\\

I rivelatori a scintillazione usati nell'esperimento sono rivelatori a Floruro di Bario ($\text{BaF}_{2}$) di diverso spessore: quello più spesso è più efficiente nel rivelare i fotoni più energetici. Il rivelatore a Floruro di Bario è un buono scintillatore per produzione di luce e linearità, in più ha un'alta densità ($\SI{4.9}{\g \per \centi\meter^{3}}$) che gli conferisce un’alta efficienza.\\
I due fotomoltiplicatori sono alimentati con una tensione di $\SI{-2}{kV}$.
Poiché il fotomoltiplicatore collegato al rivelatore sottile presenta segnali troppo elevati, ne è stata attenuata l’uscita, per poter così impostare le soglie del CFD.

\subsubsection{CFD}

Un discriminatore a frazione costante è uno strumento che dà un segnale in uscita quando il segnale di ingresso raggiunge una certa frazione del massimo e non quando supera una certa soglia. 
Infatti i segnali in uscita degli scintillatori hanno tempi di risalita identici che sono maggiori della risoluzione temporale desiderata. Questo non permette di fare un trigger temporale su una soglia fissata perchè dipenderebbe dall'altezza del picco del segnale (figura~\vref{fig:CFD} a sinistra). Tempi di risalita identici e stesse forme del segnale consentono di fare il trigger non a soglie fissate ma a frazioni costanti del picco, rendendo il trigger sul tempo indipendente dalle altezze.\\
Questo consente a segnali prodotti allo stesso istante temporale di triggerare anche nello stesso istante nonostante i segnali siano di altezze diverse.
In questo modo è possibile selezionare $\gamma$ di un certo intervallo energetico impostando un LOWER LEVEL e un UPPER LEVEL.\\
Il CFD ha diverse uscite che permettono di avere diverse forme del segnale in output: l'uscita BK OUT forma un segnale rettangolare di larghezza regolabile, quella TIMING forma un segnale piccato allo scattare del trigger.

\begin{figure}[h!tb]
	\centerline {\includegraphics[scale=0.3]{CFD.png}}
	\caption{Principio di funzionamento di un CFD.}
	\label{fig:CFD}	
\end{figure}

\subsubsection{TAC e MCA}
Il dispositivo TAC (Time to Amplitude Converter) permette di effettuare misure di tempo, come quella necessaria a noi per misurare la vita media del positronio. Esso prende in ingresso due segnali, che fungono da START e STOP, e restituisce in uscita un segnale in tensione proporzionale al tempo trascorso tra l’arrivo del segnale di START, dato dal segnale proveniente dal rivelatore spesso e quello di STOP, dato dal segnale proveniente dal rivelatore sottile.
L'uscita della TAC viene collegata all'ingresso $V_{in}$ del MCA e impostata su INTERNAL TRIGGER.

\subsection{Esperimento}

\subsubsection{Impostazione delle soglie}

La prima operazione fatta è stata quella di impostare correttamente le soglie del CFD in modo da misurare solo i $\gamma$ desiderati.
Con il rivelatore spesso andiamo a rivelare i $\gamma$ dati dalla diseccitazione del $^{22}\text{Ne}^{*}$, mentre con il rivelatore sottile quelli dati dall'annichilazione del positrone, i primi infatti sono più energetici dei secondi.
I $\gamma$ interagiscono nei nostri rivelatori soprattutto per effetto Compton, quindi quello che si ottiene negli spettri energetici sono le spalle Compton dei due $\gamma$.\\
Innanzitutto andiamo a misurare lo spettro del $^{22}\text{Na}$ separatamente con i due rivelatori, per fare questa misura abbiamo sdoppiato il segnale con un modulo di fan-in fan-out: un'uscita viene mandata al CFD che sfruttando l'uscita BK OUT fornisce un segnale rettangolare di larghezza regolabile, l'altra viene mandata a un modulo di ritardo in modo tale che il segnale in uscita dal fotomoltiplicatore sia temporalmente contenuto in quello del CFD. Entrambi i segnali vengono mandati al MCA, il segnale proveniente dal CFD nell'ingresso EXT. TRIGGER, quello proveniente dai fotomoltiplicatori in $Q_{in}$. Durante questa operazione le soglie sono aperte in modo da misurare tutti i $\gamma$.

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}[auto, node distance=3cm and 1cm,>=latex',  L/.style = {draw, red, -{Stealth[scale=3,length=3,width=2]}}, T/.style = {draw, rounded corners, to path={|- (\tikztotarget)}}, T1/.style = {draw, rounded corners, to path={-| (\tikztotarget)}}]
	
	\node [block] (fotomoltipl) {Fotomoltipl.};
	\node [block, right of=fotomoltipl] (fan) {Fan-in Fan-out};
	\node [block, right of=fan] (discr) {CFD};
	\node [block, right of=discr] (mca) {MCA};
	
	
	\draw [->] (fotomoltipl)--(fan);
	\draw [->] (fan)--(discr);
	\draw [->] (discr)--(mca);
%	\draw[->] ([yshift= 0.3 cm]discr.east) to  ([yshift= 0.3 cm]coin.west) ;
%	\draw[->] ([yshift= -0.3 cm]discr.east) to  ([yshift= -0.3 cm]coin.west) ;
%	
%	\draw[->] ([yshift= 0.3 cm]preamp.east) to  ([yshift= 0.3 cm]discr.west) ;
%	\draw[->] ([yshift= -0.3 cm]preamp.east) to  ([yshift= -0.3 cm]discr.west) ;
	
%	\draw[->] (coin) -- (mca);
	%\draw (preamp) -- (3,-1.5);
	%\draw (3,-1.5) -- (12,-1.5);
	%\draw[->] (12,-1.5) -- (mca);
	%\draw (preamp) to[line to] ;
	
%	\node[block, below of=mca](dt){Dual Timer};
	\node[block, below of=discr](rit){Delay};
	\draw (fan) edge[T, ->] (rit);
	\draw (rit) edge[T1, ->] (mca);
	
	
	\end{tikzpicture}
	\caption{Collegamenti delle unità della catena elettronica per l'impostazione delle soglie.}
	\label{fig:catena}
\end{figure}


La spalla Compton di interesse per il rivelatore sottile è quella evidenziata in figura \vref{plot:rivelatore_sottile2} a cui corrispondono i  $\gamma$ alle energie di \SI{511}{keV}: essi forniscono il segnale di STOP poiché dovuti all'annichilazione del positrone.\\
Lo spettro del rivelatore spesso mostra anch'esso due spalle Compton, quella di interesse è quella a più alta energia dovuta alla diseccitazione del $^{22}\text{Ne}^{*}$ che rappresenta il segnale di START (figura~\vref{plot:rivelatore_spesso}).\\

A causa delle esigue dimensioni dei rivelatori non sono presenti fotopicchi.


%\begin{figure}[htbp]
%	\centering
%	\begin{tikzpicture}
%	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=0]
%	\addplot [smooth, color=blue] file {dati_positronio/sottile.txt};
%	\end{axis}
%	\end{tikzpicture}
%	\caption{Spettro ottenuto da rivelatore sottile}
%	\label{plot:rivelatore_sottile}
%\end{figure}

%\begin{figure}[htbp]
%	\centering
%	\begin{tikzpicture}
%	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=0, xmax=1200]
%	\addplot [smooth, color=blue] file {dati_positronio/sottile_traslato.txt};
%	\end{axis}
%	\end{tikzpicture}
%	\caption{Spettro ottenuto da rivelatore sottile traslato}
%	\label{plot:rivelatore_sottile_traslato}
%\end{figure}

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}
	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=0, xmax=1000, ymin=0, ymax=800]
	
	\addplot[name path=f, domain=170:310]{800};
	\addplot[name path=axis, domain=170:310]{0};
	
	\addplot [
	thick,
	color=blue,
	fill=blue, 
	fill opacity=0.05
	]
	fill between[
	of=f and axis,
	soft clip={domain=170:310},
	];
	
%	\fill[blue!40]
%	(axis cs:170,-73) -- 
%	(axis cs:330,-73) --
%	(axis cs:330,750) --
%	(axis cs:170,750) --
%	cycle;
	
	\addplot [smooth, color=blue] file {dati_positronio/sottile2.txt};
	\end{axis}
	\end{tikzpicture}
	\caption{Spettro ottenuto da rivelatore sottile.}
	\label{plot:rivelatore_sottile2}
\end{figure}


\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}
	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=0, xmax=600, ymin=0, ymax=1400]
	
	\addplot[name path=f, domain=110:230]{1400};
	\addplot[name path=axis, domain=110:230]{0};
	
	\addplot [
	thick,
	color=blue,
	fill=blue, 
	fill opacity=0.05
	]
	fill between[
	of=f and axis,
	soft clip={domain=110:240},
	];
	
	\addplot [smooth, color=blue] file {dati_positronio/spesso.txt};
	\end{axis}
	\end{tikzpicture}
	\caption{Spettro ottenuto da rivelatore spesso.}
	\label{plot:rivelatore_spesso}
\end{figure}


\subsubsection{Calibrazione Temporale con sorgente di cobalto}
Per ottenere una misura temporale è necessario convertire i canali in tempo.
Per fare la calibrazione canali-tempo abbiamo utilizzato una sorgente di $^{60}\text{Co}$, il quale decade emettendo due $\gamma$ che si possono considerare in concidenza, in quando distanziati di un tempo dell'ordine dei picosecondi.
La catena elettronica in figura \vref{fig:calibrazione_cobalto} permette di regolare il ritardo relativo tra i segnali dei due fotoni.
I due segnali vengono mandati a un TAC che permette di visualizzare lo spettro della loro differenza temporale.

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}[auto, node distance=3cm and 1cm,>=latex',  L/.style = {draw, red, -{Stealth[scale=3,length=2.5,width=2]}}, T/.style = {draw, rounded corners, to path={|- (\tikztotarget)}}, T1/.style = {draw, rounded corners, to path={-| (\tikztotarget)}}]
	
	\node [block] (scintillatore) {Rivelatore};
	\node [block, right of=scintillatore] (fot) {PMT + Att};
	\node [block, right of=fot] (cfd) {CFD};
	\node [block, right of=cfd] (tac) {TAC};
	\node [block, right of=tac] (mca) {MCA};
	
	
	\draw [->] (scintillatore)--(fot);
	\draw [->] (fot)--(cfd);
	\draw [->] (cfd)--(tac);
	\draw [->] (tac)--(mca);
	
	\node[block, below of=tac](delay){Delay};
	\node[block, below of=cfd](cfd2){CFD};
	\node[block, below of=fot](fot2){PMT};
	\node[block, below of=scintillatore](scintillatore2){Rivelatore};
	\draw [->] (scintillatore2)--(fot2);
	\draw [->] (fot2)--(cfd2);
	\draw [->] (cfd2)-- (delay);
	\draw [->] (delay)--(tac);
	\end{tikzpicture}
	\caption{Catena elettronica per la calibrazione temporale con $^{60}Co$.}
	\label{fig:calibrazione_cobalto}
\end{figure}

Si acquisiscono i vari spettri impostando, attraverso un unità di ritardo, i seguenti ritardi \SI{0}{ns}, \SI{5}{ns}, \SI{9}{ns}, \SI{13}{ns}, \SI{17}{ns}.\\
Ogni spettro viene fittato con una gaussiana, il cui valor medio corrisponde al canale relativo a quello sfasamento temporale, come mostrato in figura~\vref{plot:cobalto_gaus}.

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}
	\begin{axis}[xlabel=Channel, ylabel=N, y unit=\si{\au}, xmin=900]
	\addplot [smooth] file {dati_positronio/cobalto1.txt};
	\addplot [smooth] file {dati_positronio/cobalto2.txt};
	\addplot [smooth] file {dati_positronio/cobalto3.txt};
	\addplot [smooth] file {dati_positronio/cobalto4.txt};
	\addplot [smooth] file {dati_positronio/cobalto5.txt};
	\addplot [smooth] file {dati_positronio/cobalto6.txt};
	\addplot [smooth] file {dati_positronio/cobalto7.txt};
	
	\addplot[smooth, red, domain=900:1200, samples=100] {gauss(23.6357, 1052.98, 30.6518)};
	\addplot[smooth, yellow, domain=1000:1150, samples=100] {gauss(28.294, 1084.61, 14.5769)};
	\addplot[smooth, blue, domain=1050:1250, samples=100] {gauss(19.2567, 1136.19, 20.152)};
	\addplot[smooth, green, domain=1200:1400, samples=100] {gauss(14.8074, 1286.94, 22.5453)};
	\addplot[smooth, pink, domain=1450:1650, samples=100] {gauss(15.3132, 1538.31, 21.1838)};
	\addplot[smooth, cyan, domain=1600:1800, samples=100] {gauss(22.8803, 1686.77, 18.1683)};
	\addplot[smooth, magenta, domain=1700:2000, samples=100] {gauss(18.0869, 1838.4, 18.9137)};
	\end{axis}
	\end{tikzpicture}
	\caption{Fit gaussiani per i $\gamma$ del cobalto con ritardi di \SI{0}{ns}, \SI{5}{ns}, \SI{9}{ns}, \SI{13}{ns}, \SI{17}{ns}.}
	\label{plot:cobalto_gaus}
\end{figure}

 Successivamente le coppie di ritardi temporali e canali, ricavati dal fit, sono state fittate con una retta per ottenere la conversione canali-energia, come riportato nel grafico ~\vref{plot:cobalto_fit}. Otteniamo la seguente retta di calibrazione:

\begin{equation}
\text{canale} = (\SI{50(1)}{}) \cdot \text{t}[\si{\nano\second}] + (\SI{1038(11)}{})
\label{retta_calibrazione}
\end{equation}


\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}
	\begin{axis}[xlabel=$\Delta t$, ylabel= Channel, , xmin=-1, xmax=17]
	\addplot [smooth, only marks, mark=o,error bars/.cd, y dir=both,y explicit] table[x=x, y=y, y error=erry] {dati_positronio/Cobalto_Fit/COBALTO.dat};
	
	\addplot[smooth, red, domain=0:16, samples=100] {50*x + 1038};
	\end{axis}
	\end{tikzpicture}
	\caption{Retta di calibrazione canali-tempo con sorgente di cobalto.}
	\label{plot:cobalto_fit}
\end{figure}

\subsubsection{Analisi Dati}
Fissate le soglie abbiamo fatto partire la misura della vita media del positronio.
La catena elettronica utilizzata nell'acquisizione dati è quella riportata in figura \vref{fig:vita_media_positronio}.
Le uscite TIMING dei CDF sono state mandate ad un modulo TAC il quale fornisce in output un segnale di ampiezza proporzionale alla distanza temporale tra i due segnali in ingresso. Il segnale in uscita dal TAC è stato collegato all’entrata $V_{in}$ del MCA, il quale è stato impostato su INTERNAL TRIGGER.\\
Lo spettro ottenuto viene analizzato attraverso RooFit; in particolare fittiamo la curva con la somma di due esponenziali e con una funzione gaussiana che tiene conto della risposta temporale del rivelatore.
Il primo esponenziale corrisponde all'annichilazione del positrone, la seconda al decadimento dovuto alla reazione di pickoff.
Il fit totale è riportato nell'equazione \vref{eq:fit} dove abbiamo lasciato come parametri liberi $A$, $B$, $\tau_{1}$, $\tau_{2}$ e $\sigma$.\\

\begin{equation}
y(t)=\int_{-\infty}^{+\infty} dt' \, \left( Ae^{-\frac{t'}{\tau_{1}}} + Be^{-\frac{t'}{\tau_{2}}} \right) G(t',t,\sigma)
\label{eq:fit}
\end{equation}

Il fit ottenuto è riportato in figura \vref{fig:positronio}; esso è stato fatto in canali ed è stata poi effettuata la conversione in tempo dei parametri $\tau_{1}$ e $\tau_{2}$. 

I risultati ottenuti per $\tau_{1}$ e $\tau_{2}$ sono:
$$\tau_{1}=\SI{0,464(6)}{\nano\second}$$
$$\tau_{2}=\SI{0,910(7)}{\nano\second}$$

Il peso relativo della componente veloce è pari a 0,87 $\pm$ 0,05.
Il fit è in buon accordo con i dati sperimentali dal momento che segue l'andamento generale della curva, tuttavia si presenta una discrepanza sul picco della curva dal momento che non viene fittato interamente.
Per ottenere un fit migliore sarebbe necessario un modello più elaborato rispetto alla convoluzione di una somma di esponenziali con una gaussiana. 

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.6]{positronio_canali_giusto.pdf}
	\includegraphics[scale=0.6]{positronio_canali_giusto_log.pdf}
	\caption{Nel primo grafico è presentato il fit dello spettro temporale del positronio, e nel secondo è stato raffigurato in scala logaritmica.}
	\label{fig:positronio}
\end{figure}	









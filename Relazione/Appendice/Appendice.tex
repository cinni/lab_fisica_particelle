\section{Curva di Landau}\label{sec:landau_app}

Consideriamo un particella di energia $E_0$ che passa attraverso un materiale di spessore $x$, la densità di probabilità per unità di lunghezza che la particella ha di rilasciare una energia $\epsilon$ sarà una generica funzione $w \left(E, \epsilon\right)$. Se consideriamo che l'energia rilasciata è molto inferiore a quella della particella ($\epsilon \ll E$) possiamo approssimare l'energia con una costante $E = E_0$, ottenendo $w \left(E, \epsilon\right) \sim w \left(E_0, \epsilon\right) = w \left(\epsilon\right)$.

Per ricavare la distribuzione di Landau $f\left( x, \delta\right)$, ossia la densità di probabilità di avere un deposito di energia $\delta$ in uno spessore $x$, ricaviamo l'integrale collisionale.

Consideriamo una generica distribuzione per la funzione $f$, consideriamo il bin di energia $\delta$ e delle particelle che rilasciano un'energia $\epsilon$, i casi che contribuiscono ad una variazione di probabilità in questo intervallo sono le particelle che erano nell'intervallo $ \delta - \epsilon$  e quelle che erano nel bin $\delta$. La variazione totale sarà dato dal contributo delle prime particelle (che passano al bin $\delta$) meno quello delle seconde (che escono dal bin $\delta$). Quindi l'integrale collisionale sarà dato da due termini:

\begin{itemize}
	\item $w \left(\epsilon\right) f\left( x, \delta - \epsilon \right)$ rappresenta le particelle che partendo con un'energia $\delta - \epsilon$ ne perdono $\epsilon$.
	\item $w \left(\epsilon\right) f\left( x, \delta\right)$  rappresenta le particelle che partendo con un'energia $\delta$ ne perdono $\epsilon$.
\end{itemize}

Ricordando che la funzione $w \left(\epsilon\right)$ è la pdf per unità di lunghezza, scriviamo l'integrale collisionale riferito a $\partial f/ \partial x$:

\begin{equation}\label{eq:collisional_app}
\frac{\partial}{\partial x} f\left(x,\delta \right) = \int_{0}^{+\infty}{ w\left(\epsilon\right) \left[f\left(x,  \delta - \epsilon \right)  - f\left(x,\delta \right)\right] d\epsilon}
\end{equation}

Notiamo che l'estremo di superiore di integrazione superiore è $E_0$, ma considerando che $w \left(\epsilon\right) = 0 $ per $\epsilon > E_0$, l'integrale può essere facilmente esteso all'infinito.

La risoluzione dell'equazione differenziale~\vref{eq:collisional_app} può essere fatta utilizzando la trasformata di Laplace.

\begin{equation} \label{eq:def_laplace_app}
	\varphi \left(x, p\right) := L \left[ f\left(x, \delta\right)\right] = \int_{0}^{+\infty} f\left(x, \delta\right) e^{-p \delta}d\delta
\end{equation}

Utilizzando le proprietà delle trasformate $L \left[ f\left(x,\delta - \epsilon \right) \right] = \varphi \left(x, p\right) e^{-\epsilon p}$ si ottiene:

\begin{equation}\label{eq:laplace_app}
  \frac{\partial}{ \partial x} \varphi \left(x, p\right) = - \varphi \left(x, p\right) \int_{0}^{+ \infty} w\left(\epsilon\right) \left( 1 - e^{-p \epsilon}\right) d\epsilon
\end{equation}

Per risolvere l'equazione~\vref{eq:laplace_app} bisogna fissare la condizione al contorno per $x=0$. Notiamo che deve valere la condizione $f \left(0, \Delta \right) = \delta(\Delta)$, essa è equivalente (usando la relazione~\vref{eq:def_laplace_app}) a $\varphi \left(0, p\right) = 1$.

\begin{equation}\label{eq:laplace_sol_app}
\varphi \left(x, p\right) = \exp\left[- x \int_{0}^{+ \infty} w\left(\epsilon\right) \left( 1 - e^{-p \epsilon}\right) d\epsilon \right]
\end{equation}

Infine per trovare la soluzione in funzione delle variabili di partenza utilizziamo l'antitrasformata ottenendo l'equazione~\vref{eq:sol_app}, dove abbiamo indicato $I \left(p\right)$ l'integrale che comprare ad esponente.

\begin{equation}\label{eq:sol_app}
 f\left(x, \delta\right) = \frac{1}{2 \pi i} \int_{+i \infty + \sigma}^{ -i \infty + \sigma} e^{p \delta - x I \left(p\right)} dp
\end{equation}

Il problema dal punto di vista formale sembra risolto, ma per ottenere un'espressione esplicita bisogna trovare una funzione per descrivere $ w\left(\epsilon\right)$. Definiamo $\epsilon_0$ come l'energia media di legame degli elettroni e chiamiamo $\epsilon_{max}$ la massima energia che una particella può cedere ad un elettrone per ionizzazione. \`E ragionevole assumere che i valori di $p$ che hanno un contributo rilevante nell'integrale $I \left(p\right)$ siano quelli tale che:

\begin{equation}
p \epsilon_0 \ll 1 \quad \quad p\epsilon_{max} \gg 1
\end{equation}

L'integrale può essere spezzato in due parti dividendo il dominio di integrazione. Se si considera $\epsilon_0 < \epsilon_1 < \epsilon_{max}$ e si divide l'integrale utilizzando $\epsilon_1$, si ottiene:

\begin{align*}
	I \left(p\right) &= \int_{0}^{+ \infty} w\left(\epsilon\right) \left( 1 - e^{-p \epsilon}\right) d\epsilon = \\
	&= p \int_{0}^{\epsilon_1} w\left(\epsilon \right) \epsilon \, d\epsilon +
	 \int_{\epsilon_1}^{+ \infty} w\left(\epsilon\right) \left( 1 - e^{-p \epsilon}\right) d\epsilon 
\end{align*}

Nel primo integrale abbiamo considerato che $p\epsilon_1 \ll 1$ e quindi approssimato il termine $1 - \exp(-p \epsilon) \simeq 1 - (1 - p \epsilon) = p \epsilon$, mentre nel secondo non sono state fatte approssimazioni. Per i valori di rilascio di energia tali che $\epsilon \in \left[\epsilon_0, \epsilon_{max}\right]$ vale la relazione di Bethe-Bloch~\vref{eq:bethe1_app} dove i parametri fondamentali sono raccolti nella costante $k$ nell'equazione~\vref{eq:bethe2_app}. Nella tabella~\vref{tab:bethe_app} sono riportati i parametri fondamentali presenti nella formula.

\begin{align}\label{eq:bethe1_app}
 &w\left(\epsilon\right) = \frac{k}{\epsilon^2} \\
 \label{eq:bethe2_app}
 &k= \frac{2 \pi N e^4 \rho \sum Z}{m v^2 \sum A} 
\end{align}


\begin{table}[htbp]
	\centering
	\begin{tabular}{|c|c|}
	\hline 
	$N$ & Numero di Avogadro \\ 
	\hline 
	$e$ & Carica della particella \\ 
	\hline 
	$\rho$ & Densità del materiale \\ 
	\hline 
	$\sum Z$ & Elettroni dell'atomo \\ 
	\hline 
	$m$ & Massa della particella \\ 
	\hline 
	$v$ & Velocità particella \\ 
	\hline 
	$\sum A$ & Massa atomica \\ 
	\hline 
\end{tabular} 
\caption{}
\label{tab:bethe_app}
\end{table}

Risolvendo l'integrale $I \left(p\right)$ si ottiene:

\begin{align}
\label{eq:int1_app}
&\int_{\epsilon'}^{\epsilon_1} \frac{k}{\epsilon^2} \epsilon \, d\epsilon = k \ln \left( \frac{\epsilon_1}{\epsilon'}\right) \\
\label{eq:int2_app}
&\int_{\epsilon_1}^{+\infty} k \frac{\left(1 - e^{-p \epsilon}\right)}{\epsilon^2} d\epsilon = k p(1 - C - \ln p\epsilon_1)
\end{align}

Si noti che l'espressione fornita per $w\left(\epsilon\right)$ vale a partire da $\epsilon'$, l'energia minima fornita al materiale dalla particella, ed è nulla per energie inferiori. In questo modo è possibile risolvere l'integrale~\vref{eq:int1_app} che altrimenti sarebbe divergente in zero. In particolare per il modello scelto si ha l'equazione~\vref{eq:eps_min_app}, dove $I$ è il potenziale di ionizzazione medio.

\begin{equation}
\label{eq:eps_min_app}
 \ln \epsilon' = \ln \frac{\left(1 - \frac{v^2}{c^2}\right)I^2}{2 m v^2} + \frac{v^2}{c^2}
\end{equation}

Il secondo integrale invece contiene una costante $C$, detta di Eulero-Mascheroni~\footnote{
L'integrale viene prima risolto per parti: $$\int_{\epsilon_1}^{+\infty}\frac{d\epsilon}{\epsilon^2} -\int_{\epsilon_1}^{+\infty}\frac{e^{-p\epsilon}}{\epsilon^2} d\epsilon = \frac{1}{\epsilon_1} \left( 1 - e^{-p\epsilon}\right) - p \int_{\epsilon_1}^{+\infty} \frac{ e^{-p\epsilon}}{\epsilon} d\epsilon$$

Ricordando che $p\epsilon_1 \ll 1$ e definendo $z= p \epsilon $:

$$ p\left(1 -  \int_{p\epsilon_1}^{+\infty} \frac{ e^{-z}}{z} dz \right) = p\left(1 - \int_{1}^{+\infty} \frac{ e^{-z}}{z} dz  -  \int_{0}^{1} \frac{ e^{-z}}{z} dz - \int_{0}^{1} \frac{ dz}{z} + \int_{p \epsilon_1}^{1} \frac{dz}{z} \right) $$

Considerando opportuni raccoglimenti otteniamo:
 $$ p\left(1 - \ln(p\epsilon_1) - \int_{0}^{+\infty} \frac{ e^{-z}}{z} dz  - \int_{0}^{1} \frac{ dz}{z} \right) = p \left(1 - \ln(p\epsilon_1) - C\right)$$
 Dove $C$ è la costante di Eulero-Mascheroni.
}. Riassumendo l'integrale assume la forma:

\begin{equation}
I \left(p\right) = k p \left[ \ln \left(\frac{\epsilon_1}{\epsilon'}\right) + 1  - C - \ln p \epsilon_1\right]
\end{equation}

\begin{align*}
f\left(x, \delta \right) &= \frac{1}{2 \pi i} \int_{+i \infty + \sigma}^{ -i \infty + \sigma} \exp \left\{p \left[\delta - kx \left( \ln \left(\frac{\epsilon_1}{\epsilon'}\right) + 1  - C - \ln p \epsilon_1 \right) \right] \right\} dp = \\
&= \frac{1}{2 \pi i} \int_{+i \infty + \sigma}^{ -i \infty + \sigma} \exp \left\{p \left[\delta - kx \left( \ln \left(\frac{1}{p \epsilon'}\right) + 1  - C  \right) \right] \right\} dp = \\
&= \frac{1}{2 \pi i} \int_{+i \infty + \sigma}^{ -i \infty + \sigma} \exp \left\{\left[\frac{\delta (pkx)}{kx} - (pkx)\left(-\ln (pkx) + \ln \left(\frac{pkx}{p \epsilon'}\right) + 1  - C  \right) \right] \right\} dp
\end{align*}
A questo punto è opportuno fare un cambio di variabili introducendo $\xi = kx$ e $u= p\xi=pkx$, in particolare con il cambio di variabili si ha che $dp = du/\xi$.
\begin{equation}
f\left(u, \xi \right)= \frac{1}{\xi}\frac{1}{2 \pi i} \int_{+i \infty + \sigma}^{ -i \infty + \sigma} \exp \left[ u \ln u + u \left( \frac{\delta}{\xi} - \ln \left(\frac{\xi}{ \epsilon'}\right) +1 -C \right) \right] du 
\end{equation}

Raggruppando la parte contenete le variabili $\xi$ si ottiene la distribuzione di Landau~\vref{eq:andau_app}.
\begin{align}\label{eq:andau_app}
& f\left(u, \lambda \right)=\frac{1}{\xi} \frac{1}{2 \pi i} \int_{+i \infty + \sigma}^{ -i \infty + \sigma} e^{ u \ln u + \lambda u} du \\
& \lambda = \frac{\delta}{\xi} - \ln \left(\frac{\xi}{ \epsilon'}\right) +1 -C 
\end{align}

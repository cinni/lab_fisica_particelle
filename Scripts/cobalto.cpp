#include <iostream>
#include <fstream>
#include "TH1F.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TMath.h"
//#include <"TF1Convolution.h">
#include <iostream>
#include <cstdlib>
#include <vector>
#include "string.h"


using namespace std;

//LETTURA FILE da TERMINALE

 double landau (double* x, double* par){
 double N = par[0];
 double mean = par[1];
 double sigma = par[2];
 return N*TMath::Landau(x[0],mean,sigma);
 }

 double rumore (double* x, double* par){
 double N = par[0];
 double mean = par[1];
 double sigma = par[2];
 return N*TMath::Gaus(x[0],mean,sigma);
 }

 double totale_prova (double* x,double* par){
 return landau(x,par) + rumore(x,par);
 }

 double totale (double* x,double* par){
 double Nl = par[0];
 double meanl = par[1];
 double sigmal = par[2];
 double Nr = par[3];
 double meanr = par[4];
 double sigmar = par[5];
 return Nl*TMath::Landau(x[0],meanl,sigmal) + Nr*TMath::Gaus(x[0],meanr,sigmar);
 }

 double background (double* x,double* par){
 double Nr = par[0];
 double meanr = par[1];
 double sigmar = par[2];
 double Nr2 = par[3];
 double meanr2 = par[4];
 double sigmar2 = par[5];
 double Nsp = par[6];
 double meansp = par[7];
 double sigmasp = par[8];
 return Nr*TMath::Gaus(x[0],meanr,sigmar) + Nr2*TMath::Gaus(x[0],meanr2,sigmar2) + Nsp*TMath::Gaus(x[0],meansp,sigmasp);
 }

 double totale2 (double* x,double* par){
 double Nl = par[0];
 double meanl = par[1];
 double sigmal = par[2];
 double Nr = par[3];
 double meanr = par[4];
 double sigmar = par[5];
 double Nr2 = par[6];
 double meanr2 = par[7];
 double sigmar2 = par[8];
 return Nl*TMath::Landau(x[0],meanl,sigmal) + Nr*TMath::Gaus(x[0],meanr,sigmar) +   Nr2*TMath::Gaus(x[0],meanr2,sigmar2);
 }

int main(int argc, char** argv){
  TApplication app("app",0,0);

//CARICAMENTO DATI

  ifstream in1("cobalto1.txt");
  ifstream in2("cobalto2.txt");
  ifstream in3("cobalto3.txt");
  ifstream in4("cobalto4.txt");
  ifstream in5("cobalto5.txt");
  ifstream in6("cobalto6.txt");
  ifstream in7("cobalto7.txt");
  
 int N1=0;
 int N2=0;
 int N3=0;
 int N4=0;
 int N5=0;
 int N6=0;
 int N7=0;
      
 vector<double> a1;
 vector<double> a2;
 vector<double> a3;
 vector<double> a4;
 vector<double> a5;
 vector<double> a6;
 vector<double> a7;
 double x;
 
 while ( in1 >> x ){
   in1>>x;
   a1.push_back(x);
 }
while ( in2 >> x ){
   in2>>x;
   a2.push_back(x);
 }
 while ( in3 >> x ){
   in3>>x;
   a3.push_back(x);
 }
 while ( in4 >> x ){
   in4>>x;
   a4.push_back(x);
 }
 while ( in5 >> x ){
   in5>>x;
   a5.push_back(x);
 }
 while ( in6 >> x ){
   in6>>x;
   a6.push_back(x);
 }
 while ( in7 >> x ){
   in7>>x;
   a7.push_back(x);
 }

 in1.close();
 in2.close();
 in3.close();
 in4.close();
 in5.close();
 in6.close();
 in7.close();
 
 N1=a1.size();
 N2=a1.size();
 N3=a1.size();
 N4=a1.size();
 N5=a1.size();
 N6=a1.size();
 N7=a1.size();

//GRAFICO DISTRIBUZIONE + FIT

 TH1F *h1 = new TH1F("Info","Segnale acquisito1",N1,0.,double(N1));
 h1->GetXaxis()->SetTitle("Channel");
 h1->GetYaxis()->SetTitle("Counts");
 //h->GetYaxis()->SetLimits(0.,20000.);
 
 for(int j=1;j<N1+1;j++){
 h1->SetBinContent(j,a1[1+j-2]);
 }
 cout << N1 << endl;
 TF1 *f1 = new TF1("normal1","[0]*TMath::Gaus(x,[1],[2])", 0., double(N1));
 f1->SetParameters(10000., 1000, 30);
 h1->Fit(f1);

 TH1F *h2 = new TH1F("Info","Segnale acquisito2",N2,0.,double(N2));
 h2->GetXaxis()->SetTitle("Channel");
 h2->GetYaxis()->SetTitle("Counts");
 //h->GetYaxis()->SetLimits(0.,20000.);
 
 for(int j=1;j<N2+1;j++){
 h2->SetBinContent(j,a2[1+j-2]);
 }
 cout << N2 << endl;
 TF1 *f2 = new TF1("normal2","[0]*TMath::Gaus(x,[1],[2])", 0., double(N2));
 f2->SetParameters(10000., 1100, 30);
 h2->Fit(f2,"","R", 1050, 1100);

 TH1F *h3 = new TH1F("Info","Segnale acquisito3",N3,0.,double(N3));
 h3->GetXaxis()->SetTitle("Channel");
 h3->GetYaxis()->SetTitle("Counts");
 //h->GetYaxis()->SetLimits(0.,20000.);
 
 for(int j=1;j<N3+1;j++){
 h3->SetBinContent(j,a3[1+j-2]);
 }
 cout << N3 << endl;
 TF1 *f3 = new TF1("normal3","[0]*TMath::Gaus(x,[1],[2])", 0., double(N3));
 f3->SetParameters(10000., 1100, 30);
 h3->Fit(f3,"" ,"R", 1000, 1200);

 TH1F *h4 = new TH1F("Info","Segnale acquisito4",N4,0.,double(N4));
 h4->GetXaxis()->SetTitle("Channel");
 h4->GetYaxis()->SetTitle("Counts");
 //h->GetYaxis()->SetLimits(0.,20000.);
 
 for(int j=1;j<N4+1;j++){
 h4->SetBinContent(j,a4[1+j-2]);
 }
 cout << N4 << endl;
 TF1 *f4 = new TF1("normal4","[0]*TMath::Gaus(x,[1],[2])", 0., double(N4));
 f4->SetParameters(10000., 1300, 30);
 h4->Fit(f4);

 TH1F *h5 = new TH1F("Info","Segnale acquisito5",N5,0.,double(N5));
 h5->GetXaxis()->SetTitle("Channel");
 h5->GetYaxis()->SetTitle("Counts");
 //h->GetYaxis()->SetLimits(0.,20000.);
 
 for(int j=1;j<N5+1;j++){
 h5->SetBinContent(j,a5[1+j-2]);
 }
 cout << N5 << endl;
 TF1 *f5 = new TF1("normal5","[0]*TMath::Gaus(x,[1],[2])", 0., double(N5));
 f5->SetParameters(10000., 1500, 30);
 h5->Fit(f5);

 TH1F *h6 = new TH1F("Info","Segnale acquisito6",N6,0.,double(N6));
 h6->GetXaxis()->SetTitle("Channel");
 h6->GetYaxis()->SetTitle("Counts");
 //h->GetYaxis()->SetLimits(0.,20000.);
 
 for(int j=1;j<N6+1;j++){
 h6->SetBinContent(j,a6[1+j-2]);
 }
 cout << N6 << endl;
 TF1 *f6 = new TF1("normal6","[0]*TMath::Gaus(x,[1],[2])", 0., double(N6));
 f6->SetParameters(10000., 1680, 30);
 h6->Fit(f6,"","R", 1650, 1700);

 TH1F *h7 = new TH1F("Info","Segnale acquisito7",N7,0.,double(N7));
 h7->GetXaxis()->SetTitle("Channel");
 h7->GetYaxis()->SetTitle("Counts");
 //h->GetYaxis()->SetLimits(0.,20000.);
 
 for(int j=1;j<N7+1;j++){
 h7->SetBinContent(j,a7[1+j-2]);
 //cout <<j<<" "<<a7[1+j-2]<<endl;
 }
 cout << N7 << endl;
 TF1 *f7 = new TF1("normal7","[0]*TMath::Gaus(x,[1],[2])", 0., double(N7));
 f7->SetParameters(10000., 1790, 30);
 h7->Fit(f7, "", "R", 1780, 1900);

 std::vector<double> X(7);
 X={0,1,2,5,10,13,16};
 
 std::vector<double> Y(7);
 Y = {f1->GetParameter(1), f2->GetParameter(1), f3->GetParameter(1), f4->GetParameter(1), f5->GetParameter(1), f6->GetParameter(1), f7->GetParameter(1)  };
 std::vector<double> errY(7);
 errY = {f1->GetParameter(2), f2->GetParameter(2), f3->GetParameter(2), f4->GetParameter(2), f5->GetParameter(2), f6->GetParameter(2), f7->GetParameter(2) };

 std::vector<double> normies(7);
 normies = {f1->GetParameter(0), f2->GetParameter(0), f3->GetParameter(0), f4->GetParameter(0), f5->GetParameter(0), f6->GetParameter(0), f7->GetParameter(0) };
 

 double s=0;
 double ds=0.1;
 
 ofstream aut;
 
 aut.open("gaus1.dat");
 s=800;
 while(s<1300) {
   aut << s << " " << f1->Eval(s) << endl;
   s+=ds;
 }
 aut.close();

 aut.open("gaus1.dat");
 s=800;
 while(s<1300) {
   aut << s << " " << f1->Eval(s) << endl;
   s+=ds;
 }
 aut.close();
 
 aut.open("gaus2.dat");
 s=900;
 while(s<1300) {
   aut << s << " " << f2->Eval(s) << endl;
   s+=ds;
 }
 aut.close();

 aut.open("gaus3.dat");
 s=900;
 while(s<1400) {
   aut << s << " " << f3->Eval(s) << endl;
   s+=ds;
 }
 aut.close();

 aut.open("gaus4.dat");
 s=1100;
 while(s<1500) {
   aut << s << " " << f4->Eval(s) << endl;
   s+=ds;
 }
 aut.close();

 aut.open("gaus5.dat");
 s=1400;
 while(s<1700) {
   aut << s << " " << f5->Eval(s) << endl;
   s+=ds;
 }
 aut.close();

 aut.open("gaus6.dat");
 s=1500;
 while(s<1900) {
   aut << s << " " << f6->Eval(s) << endl;
   s+=ds;
 }
 aut.close();

 aut.open("GAUSS_PARAMETRI.dat");
 aut << "MEAN DEV_STD" << endl;
 for(int j=0; j<7; j++) aut << normies[j] <<" " << Y[j] << " " << errY[j] << endl;
 aut.close();
 
 //FIT FINALE//
 TGraphErrors *hf = new TGraphErrors(7,&X[0],&Y[0],0 ,&errY[0]);
 hf->GetXaxis()->SetTitle("deltaT");
 hf->GetYaxis()->SetTitle("N");

 TF1 *ff = new TF1("fitf", "[0]*x + [1]",800,2000);
 ff->SetParameters(1,1);
 hf->Fit(ff);

 ofstream out("COBALTO.dat");
 for(int j=0; j<7; j++) out << X[j] << " " << Y[j] <<" " << errY[j] << endl;
 out.close();

 ofstream out1("COBALTO_FIT_FORULA.txt");
 out1 << "y = a x + b" << endl;
 out1 << "a=50 pm 1.244" << endl;
 out1 << "b= 1038 pm 11.33" << endl;
 out1.close();

 ofstream out2("COBALTO_FIT.dat");
 double dt=0.01;
 double T=0;
 while(T<=16){
   out2 << T << " " << ff->Eval(T) << endl;
   T+=dt;
 }
 /*
 //PRINT RESULTS
 ofstream output(strcat(argv[2], "_fit.dat"));
 for(int i=0; i<N; i++) output << i << " " << landauu->Eval(i)/norm<< endl;
 output.close();

 ofstream output1(strcat(argv[2],".dat"));
 for(int i=0h1->Draw();; i<N; i++) output1 << i << " " << a[i]/norm << endl;
 output1.close();
*/
 
 //HISTO OPTIONS
 TCanvas *c1 = new TCanvas("c1","c1");
 c1->cd();
 c1->SetGrid();
 h1->SetFillStyle(3003);
 h1->SetFillColor(kBlue);
 h2->SetFillColor(kRed);
 h3->SetFillColor(kYellow);
 h4->SetFillColor(kGreen);
 h5->SetFillColor(kGray);
 h6->SetFillColor(kOrange);
 h7->SetFillColor(kPink);

 h1->Draw();
 h2->Draw("SAME");
 h3->Draw("SAME");
 
 h4->Draw("SAME");
 h5->Draw("SAME");
 h6->Draw("SAME");
 h7->Draw("SAME");
 
 f1->Draw("SAME");
 f2->Draw("SAME");
 f3->Draw("SAME");

 f4->Draw("SAME");
 f5->Draw("SAME");
 f6->Draw("SAME");
 f7->Draw("SAME");

 TCanvas *c2 = new TCanvas("c2","c2");
 c2->cd();
 c2->SetGrid();
 hf->SetMarkerStyle(24);
 hf->Draw("ap");
 
 //landauu->Draw("SAME");

 //LINE OPTIONS
 /*
 rumoreTF1->Draw("same");//componente RUMORE del TOTALE (BLU)
 rumoreTF1->SetLineColor(kBlue); 
 rumore2TF1->Draw("same");//componente RUMORE del TOTALE (BLU)
 rumore2TF1->SetLineColor(kGreen); 
 spTF1->Draw("same");//componente RUMORE del TOTALE (BLU)
 spTF1->SetLineColor(kBlack); 


 //LEGENDA
 TLegend* leg = new TLegend(0.55,0.65,0.85,0.85);
 leg->AddEntry(h,"Data");
 leg->AddEntry(rumoreTF1,"Background_1 Fit","l");
 leg->AddEntry(rumore2TF1,"Background_2 Fit","l");
 leg->AddEntry(spTF1,"Background_SP Fit","l");
 leg->AddEntry(backgroundTF1,"Global Background Fit","l");
 leg->Draw();

*/

 //STAT OPTIONS
 //gStyle->SetOptStat("ne");

 app.Run();
 //chiudere canvas
 return 0;
}

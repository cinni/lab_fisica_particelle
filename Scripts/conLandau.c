//file name: convolution.C

#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooFFTConvPdf.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TH1.h"
#include "RooEffProd.h"

using namespace RooFit ;

void conLandau(){
  
  //create histogram from data file
  float y;
  int i=0;
  Int_t yc;
  
  FILE *f;
  
  //data file name here
  f=fopen("1p.dat","r");
  
  TH1F *h = new TH1F("h","Vita media",2048,0,2048);
  
  while (fscanf(f,"%f", &y)!=EOF){
    i++;
    yc=y;
    h->SetBinContent(i,y);
  }
  
  // Construct real observable
  RooRealVar t("t","Channel",0,2048);
  RooRealVar tt ("tt", "tt", 0, 2048);
  RooDataHist dh("dh","dh",t,Import(*h));  
   
  // Construct landau(t,ml,sl) ;
  RooRealVar nl("nl", "nl", 1, 0, 3000);
  RooRealVar ml("ml","mean landau",200,0,2048) ;
  RooRealVar sl("sl","sigma landau",30,0.1,100) ;
  RooLandau landau("lx","lx",t,ml,sl) ;
  
  RooRealVar nl1("nl1", "nl1", 1, 0, 3000);
  RooRealVar ml1("ml1","mean landau 1",600,0,2048) ;
  RooRealVar sl1("sl1","sigma landau 1",30,0.1,100) ;
  RooLandau landau1("lx1","lx1",t,ml1,sl1) ;
  
  // Construct gauss(t,mg,sg)
  RooRealVar ng("ng","ng",1, 0, 3000);
  RooRealVar mg("mg","mg",400, 0, 3000);
  RooRealVar sg("sg","sg",30,0.1,100) ;
  RooGaussian gauss("gauss","gauss",t,mg,sg) ;

   // Construct gauss(t,mg,sg)
  RooRealVar ng1("ng1","ng1",1, 0, 3000);
  RooRealVar mg1("mg1","mg1",100, 0, 3000);
  RooRealVar sg1("sg1","sg1",30,0.1,100) ;
  RooGaussian gauss1("gauss1","gauss1",t,mg1,sg1) ;
  
  //Construct sum of PDF
  RooAddPdf DoubleLandau("DoubleLandau","DoubleLandau",RooArgList(landau,landau1), RooArgList(nl,nl1));
  RooAddPdf LandauPlusGauss("LandauPlusGauss","LandauPlusGauss",RooArgList(landau,gauss), RooArgList(nl,ng));   
  
  //Construct Convolution   
  // Set #bins to be used for FFT sampling to 10000
  t.setBins(10000,"cache") ; 
  
  // Construct 2landau (x) gauss
  RooFFTConvPdf DLxg("DLxg","landau (X) gauss", t, DoubleLandau, gauss) ;
  // Construct landau (x) gauss
  RooFFTConvPdf lxg("lxg","double landau (X) gauss", t, landau, gauss) ;
  // Construct landau+gauus (x) gauss
  RooFFTConvPdf LPGxg("LPGxg","landauPgauss (X) gauss", t,LandauPlusGauss, gauss1) ;

  //FIT
  //RooFitResult *fitr = (RooFitResult*)LandauPlusGauss.fitTo(dh,Range(0.,2048),  RooFit::Save());
  RooFitResult *fitr = (RooFitResult*)lxg.fitTo(dh,Range(0.,2048),  RooFit::Save());
  //RooFitResult *fitr = (RooFitResult*)DLxg.fitTo(dh,Range(0.,2048),  RooFit::Save());
  // RooFitResult *fitr = (RooFitResult*)LPGxg.fitTo(dh,Range(0.,2048),  RooFit::Save());

  // RooDataSet *p = lxg.generate(t, 100000);
  // lxg.fitTo(*p);
  
  // PLOT
  RooPlot* frame = t.frame(Title("Landau")) ;
  // p->plotOn(frame);
  
  dh.plotOn(frame, DataError(RooAbsData::None));
  //  LPGxg.plotOn(frame);
  //LandauPlusGauss.plotOn(frame, LineColor(kGreen));
  //DLxg.plotOn(frame, LineColor(kRed));
  lxg.plotOn(frame, LineColor(kBlue));
  landau.plotOn(frame, LineColor(kGreen));
  gauss.plotOn(frame, LineColor(kRed));
  //gauss1.plotOn(frame, LineColor(kYellow));
  // LPGxg.plotOn(frame, LineColor(kBlue));

  Double_t chi2 = frame->chiSquare();
  fitr->Print("v");
  cout << "chi^2/dof = " << chi2 << endl;
  
  /*
  RooChi2Var chi2bis ("chi2", "chi2", lxg, dh);
  Double_t CHI2bis = chi2bis.getVal();
  cout << "chi^2/dof = " << CHI2bis << "/" << (h->GetNbinsX()-2.0) << endl;
  */
  new TCanvas("c","c") ;
  gPad->SetLeftMargin(0.15) ; frame->GetYaxis()->SetTitleOffset(1.4) ; frame->Draw();

} 

#include <iostream>
#include <fstream>
#include "TH1F.h"
#include "TF1.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TMath.h"
//#include <"TF1Convolution.h">
#include <iostream>
#include <cstdlib>
#include <vector>


using namespace std;

//LETTURA FILE da TERMINALE

 double landau (double* x, double* par){
 double N = par[0];
 double mean = par[1];
 double sigma = par[2];
 return N*TMath::Landau(x[0],mean,sigma);
 }

 double rumore (double* x, double* par){
 double N = par[0];
 double mean = par[1];
 double sigma = par[2];
 return N*TMath::Gaus(x[0],mean,sigma);
 }

 double totale_prova (double* x,double* par){
 return landau(x,par) + rumore(x,par);
 }

 double totale (double* x,double* par){
 double Nl = par[0];
 double meanl = par[1];
 double sigmal = par[2];
 double Nr = par[3];
 double meanr = par[4];
 double sigmar = par[5];
 return Nl*TMath::Landau(x[0],meanl,sigmal) + Nr*TMath::Gaus(x[0],meanr,sigmar);
 }

 double background (double* x,double* par){
 double Nr = par[0];
 double meanr = par[1];
 double sigmar = par[2];
 double Nr2 = par[3];
 double meanr2 = par[4];
 double sigmar2 = par[5];
 double Nsp = par[6];
 double meansp = par[7];
 double sigmasp = par[8];
 return Nr*TMath::Gaus(x[0],meanr,sigmar) + Nr2*TMath::Gaus(x[0],meanr2,sigmar2) + Nsp*TMath::Gaus(x[0],meansp,sigmasp);
 }

 double totale2 (double* x,double* par){
 double Nl = par[0];
 double meanl = par[1];
 double sigmal = par[2];
 double Nr = par[3];
 double meanr = par[4];
 double sigmar = par[5];
 double Nr2 = par[6];
 double meanr2 = par[7];
 double sigmar2 = par[8];
 return Nl*TMath::Landau(x[0],meanl,sigmal) + Nr*TMath::Gaus(x[0],meanr,sigmar) +   Nr2*TMath::Gaus(x[0],meanr2,sigmar2);
 }

 double totale3 (double* x,double* par){
 double Nl = par[0];
 double meanl = par[1];
 double sigmal = par[2];
 double Nr = par[3];
 double meanr = par[4];
 double sigmar = par[5];
 double Nl2 = par[6];
 double meanl2 = par[7];
 double sigmal2 = par[8];
 return Nl*TMath::Landau(x[0],meanl,sigmal) + Nr*TMath::Gaus(x[0],meanr,sigmar) +   Nl2*TMath::Landau(x[0],meanl2,sigmal2);
 }

int main(int argc, char** argv){
  TApplication app("app",0,0);

 if ( argc!=3) {
   cerr << argc << endl; 
   for (int i=0; i<argc; i++) cerr << i << ") " << argv[i] << endl; 
   cerr << "Usage: " << argv[0] << " filename.spk output.dat" << endl; 
   return -99;
 } 

//CARICAMENTO DATI

int N=0;

ifstream inputfile(argv[1]); 

if(!inputfile){
  cout <<"Errore: non riesco ad aprire il file " << argv[1] << endl;
return -1; 
}

 vector<double> a; //usa la classe vector, MOLTO piÃ¹ RAPIDO
 double x;
 while ( inputfile >> x ) a.push_back(x);

 inputfile.close();
 N=a.size();
 cout<<"Ci sono "<< N <<" elementi"<<endl;

 if(N==0){
   cout<<"Il file Ãš vuoto!"<<endl;
   return 1;
 }
 else if(inputfile.eof()){
 cout<<"Lettura avvenuta correttamente!"<<endl;
 }

 double norm=0;
 for(int i=0; i<N; i++) norm+=a[i];

//GRAFICO DISTRIBUZIONE + FIT

 TH1F *h = new TH1F("Info","Segnale acquisito",N,0.,double(N));
 h->GetXaxis()->SetTitle("Channel");
 h->GetYaxis()->SetTitle("Counts");
 //h->GetYaxis()->SetLimits(0.,20000.);
 
 for(int j=1;j<N+1;j++){
 h->SetBinContent(j,a[1+j-2]);
 }

 TF1 *f = new TF1("totale", totale, 0., double(N),6);
 f->SetParameters(10000., 800., 100., 10000., 700., 100.);
 //h->Fit(f);
 // h->GetFunction("totale");

 TF1 *f1 = new TF1("f1", totale2, 0., double(N),9);
 f1->SetParameters(10000., 800., 100., 10000., 700., 100., 10000., 1000., 100.);
 h->Fit(f1);
 //h->GetFunction("f1")->SetLineColor(kGreen);

 
 //PRINT RESULTS
 ofstream output(argv[2]);
  for(int i=0; i<N; i++) output << i << " " << f1->Eval(i)/norm<< endl;
 output.close();

 ofstream output2("misure_landau/bella1_norm.dat");
 for(int i=0; i<N; i++) output2 << i << " " << a[i]/norm<< endl;
 output2.close();
 
 
 //HISTO OPTIONS
 TCanvas *c1 = new TCanvas("c1","c1");
 c1->cd();
 c1->SetGrid();
 h->SetFillStyle(3003);
 h->SetFillColor(kBlue);
 h->Draw();
 //f->Draw("SAME");
 f1->Draw("SAME");

 //LINE OPTIONS
 /*
 rumoreTF1->Draw("same");//componente RUMORE del TOTALE (BLU)
 rumoreTF1->SetLineColor(kBlue); 
 rumore2TF1->Draw("same");//componente RUMORE del TOTALE (BLU)
 rumore2TF1->SetLineColor(kGreen); 
 spTF1->Draw("same");//componente RUMORE del TOTALE (BLU)
 spTF1->SetLineColor(kBlack); 


 //LEGENDA
 TLegend* leg = new TLegend(0.55,0.65,0.85,0.85);
 leg->AddEntry(h,"Data");
 leg->AddEntry(rumoreTF1,"Background_1 Fit","l");
 leg->AddEntry(rumore2TF1,"Background_2 Fit","l");
 leg->AddEntry(spTF1,"Background_SP Fit","l");
 leg->AddEntry(backgroundTF1,"Global Background Fit","l");
 leg->Draw();

*/

 //STAT OPTIONS
 //gStyle->SetOptStat("ne");

 app.Run();
 //chiudere canvas
 return 0;
}
